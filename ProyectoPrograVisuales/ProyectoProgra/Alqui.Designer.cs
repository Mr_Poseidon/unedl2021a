﻿
namespace ProyectoProgra
{
    partial class Alqui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Carselect2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Carg = new System.Windows.Forms.Button();
            this.regre = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.carro1 = new System.Windows.Forms.PictureBox();
            this.carro2 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Exa = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.carro3 = new System.Windows.Forms.PictureBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.carro4 = new System.Windows.Forms.PictureBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.carro1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Unispace", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(451, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vehiculos en renta";
            // 
            // Carselect2
            // 
            this.Carselect2.FormattingEnabled = true;
            this.Carselect2.Location = new System.Drawing.Point(52, 323);
            this.Carselect2.Name = "Carselect2";
            this.Carselect2.Size = new System.Drawing.Size(197, 23);
            this.Carselect2.TabIndex = 6;
            this.Carselect2.SelectedIndexChanged += new System.EventHandler(this.Carselect_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(1057, 607);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(195, 62);
            this.button1.TabIndex = 13;
            this.button1.Text = "Salir";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.button2.Location = new System.Drawing.Point(64, 422);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 39);
            this.button2.TabIndex = 14;
            this.button2.Text = "Rentar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Carg
            // 
            this.Carg.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Carg.Location = new System.Drawing.Point(215, 607);
            this.Carg.Name = "Carg";
            this.Carg.Size = new System.Drawing.Size(180, 64);
            this.Carg.TabIndex = 15;
            this.Carg.Text = "Cargar";
            this.Carg.UseVisualStyleBackColor = true;
            this.Carg.Click += new System.EventHandler(this.Carg_Click);
            // 
            // regre
            // 
            this.regre.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.regre.Location = new System.Drawing.Point(14, 607);
            this.regre.Name = "regre";
            this.regre.Size = new System.Drawing.Size(195, 62);
            this.regre.TabIndex = 16;
            this.regre.Text = "Regresar";
            this.regre.UseVisualStyleBackColor = true;
            this.regre.Click += new System.EventHandler(this.regre_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // carro1
            // 
            this.carro1.Location = new System.Drawing.Point(33, 130);
            this.carro1.Name = "carro1";
            this.carro1.Size = new System.Drawing.Size(242, 187);
            this.carro1.TabIndex = 18;
            this.carro1.TabStop = false;
            // 
            // carro2
            // 
            this.carro2.Location = new System.Drawing.Point(317, 130);
            this.carro2.Name = "carro2";
            this.carro2.Size = new System.Drawing.Size(242, 187);
            this.carro2.TabIndex = 19;
            this.carro2.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(331, 323);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(197, 23);
            this.comboBox1.TabIndex = 20;
            // 
            // Exa
            // 
            this.Exa.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Exa.Location = new System.Drawing.Point(98, 363);
            this.Exa.Name = "Exa";
            this.Exa.Size = new System.Drawing.Size(83, 33);
            this.Exa.TabIndex = 17;
            this.Exa.Text = "Examinar";
            this.Exa.UseVisualStyleBackColor = true;
            this.Exa.Click += new System.EventHandler(this.Exa_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button3.Location = new System.Drawing.Point(382, 363);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 33);
            this.button3.TabIndex = 21;
            this.button3.Text = "Examinar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // carro3
            // 
            this.carro3.Location = new System.Drawing.Point(631, 130);
            this.carro3.Name = "carro3";
            this.carro3.Size = new System.Drawing.Size(242, 187);
            this.carro3.TabIndex = 22;
            this.carro3.TabStop = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(655, 323);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(197, 23);
            this.comboBox2.TabIndex = 23;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button4.Location = new System.Drawing.Point(698, 363);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 33);
            this.button4.TabIndex = 24;
            this.button4.Text = "Examinar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // carro4
            // 
            this.carro4.Location = new System.Drawing.Point(926, 130);
            this.carro4.Name = "carro4";
            this.carro4.Size = new System.Drawing.Size(242, 187);
            this.carro4.TabIndex = 25;
            this.carro4.TabStop = false;
            this.carro4.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(943, 323);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(197, 23);
            this.comboBox3.TabIndex = 26;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button5.Location = new System.Drawing.Point(1000, 363);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 33);
            this.button5.TabIndex = 27;
            this.button5.Text = "Examinar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.button6.Location = new System.Drawing.Point(344, 422);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(145, 39);
            this.button6.TabIndex = 28;
            this.button6.Text = "Rentar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.button7.Location = new System.Drawing.Point(665, 422);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(145, 39);
            this.button7.TabIndex = 29;
            this.button7.Text = "Rentar";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.button8.Location = new System.Drawing.Point(968, 422);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(145, 39);
            this.button8.TabIndex = 30;
            this.button8.Text = "Rentar";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Alqui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.carro3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.carro2);
            this.Controls.Add(this.Exa);
            this.Controls.Add(this.regre);
            this.Controls.Add(this.Carg);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Carselect2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.carro4);
            this.Controls.Add(this.carro1);
            this.Name = "Alqui";
            this.Text = "Alqui";
            this.Load += new System.EventHandler(this.Alqui_Load);
            ((System.ComponentModel.ISupportInitialize)(this.carro1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carro4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Carselect;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox Carselect2;
        private System.Windows.Forms.Button Carg;
        private System.Windows.Forms.Button regre;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox carro1;
        private System.Windows.Forms.PictureBox carro2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button Exa;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox carro3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox carro4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        
    }
}