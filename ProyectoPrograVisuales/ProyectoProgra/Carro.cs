﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace ProyectoProgra
{
    public partial class Carro : Form

    {
        
        public Carro()
        {
            InitializeComponent();
            Bitmap img = new Bitmap(Application.StartupPath + @"\img\autos20.jpg");
            this.BackgroundImage = img;

        }

        private void Carro_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Regis_Click(object sender, EventArgs e)
        {
            String Auto = TxtNew.Text;
            String precio = TxtPrecio.Text;
            String ID = TxtID.Text;
            StreamWriter escribir = File.AppendText("H:\\Alquiler.txt");
            escribir.WriteLine(ID + ",   " + Auto + ",   $" + precio);
            escribir.Close();
            ListViewItem lista = new ListViewItem(TxtID.Text);
            lista.SubItems.Add(TxtNew.Text);
            lista.SubItems.Add(TxtPrecio.Text);
            listView1.Items.Add(lista);
           
            // "H:\\Alquiler.txt"

            //StreamWriter agrega = File.AppendText("H:\\Alquiler.txt");
            //agrega.WriteLine(precio);
            //agrega.Close();

            TxtNew.Clear();
            TxtPrecio.Clear();
            TxtID.Clear();
            
        }

        private void Carselect_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Del_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem lista in listView1.SelectedItems)
            {
                lista.Remove();
            }

                     
        }

        private void Alq_Click(object sender, EventArgs e)
        {
            this.Hide();
            Alqui A = new Alqui();
            A.Show();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Rec_Click(object sender, EventArgs e)
        {
            string tempFile = Path.GetTempFileName();
       using (StreamReader lector = new StreamReader("H:\\Alquiler.txt"))
            using (StreamWriter escribir = File.AppendText(tempFile))
                {
                    string line = "";
                    foreach (ListViewItem item in this.listView1.Items)
                    {
                        line = "";
                        foreach (ListViewSubItem subitem in item.SubItems)
                        {
                            line += subitem.Text + "  ";
                        }
                        escribir.WriteLine(line);
                    }
                    escribir.Close();
                }
            
            File.Delete("H:\\Alquiler.txt");
            File.Move(tempFile, "H:\\Alquiler1.txt");
        }
    }
}
