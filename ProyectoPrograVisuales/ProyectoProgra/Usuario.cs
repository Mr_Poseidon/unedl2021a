﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ProyectoProgra
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent(); 
            Bitmap img = new Bitmap(Application.StartupPath + @"\img\autos11.jpg");
            this.BackgroundImage = img;
        }

        private void btncarga_Click(object sender, EventArgs e)
        {
            using (StreamReader lector = new StreamReader("H:\\Alquiler1.txt"))
            {
                while (lector.Peek() >= 0)
                {
                    comboBox1.Items.Add(lector.ReadLine().Split(':')[0]);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String nombre = textBox1.Text;
            String apellido = textBox2.Text;
            String domicilio = textBox3.Text;
            String email = textBox4.Text;
            String fecha = textBox5.Text;
            MessageBox.Show("Su ticket: \n" + nombre +" " +apellido + "\n" + domicilio + "\n" + email + "\n" + fecha +"\n" + comboBox1.SelectedIndex); 
        }
    }
}
