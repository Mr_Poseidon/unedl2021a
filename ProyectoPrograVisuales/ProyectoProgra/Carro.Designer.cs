﻿
namespace ProyectoProgra
{
    partial class Carro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNew = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPrecio = new System.Windows.Forms.TextBox();
            this.Exit = new System.Windows.Forms.Button();
            this.Regis = new System.Windows.Forms.Button();
            this.Del = new System.Windows.Forms.Button();
            this.Alq = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ID = new System.Windows.Forms.ColumnHeader();
            this.Vehi = new System.Windows.Forms.ColumnHeader();
            this.Prec = new System.Windows.Forms.ColumnHeader();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtID = new System.Windows.Forms.TextBox();
            this.Rec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Unispace", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(395, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(437, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registro de vehiculos";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TxtNew
            // 
            this.TxtNew.Location = new System.Drawing.Point(959, 169);
            this.TxtNew.Name = "TxtNew";
            this.TxtNew.Size = new System.Drawing.Size(190, 23);
            this.TxtNew.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(12, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(238, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Seleccione Vehiculo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(959, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(190, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nombre Vehiculo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(959, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 23);
            this.label4.TabIndex = 5;
            this.label4.Text = "Precio";
            // 
            // TxtPrecio
            // 
            this.TxtPrecio.Location = new System.Drawing.Point(959, 267);
            this.TxtPrecio.Name = "TxtPrecio";
            this.TxtPrecio.Size = new System.Drawing.Size(190, 23);
            this.TxtPrecio.TabIndex = 6;
            // 
            // Exit
            // 
            this.Exit.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Exit.Location = new System.Drawing.Point(13, 599);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(159, 63);
            this.Exit.TabIndex = 7;
            this.Exit.Text = "Salir";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Regis
            // 
            this.Regis.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Regis.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Regis.Location = new System.Drawing.Point(278, 599);
            this.Regis.Name = "Regis";
            this.Regis.Size = new System.Drawing.Size(226, 68);
            this.Regis.TabIndex = 8;
            this.Regis.Text = "Registrar Vehiculo";
            this.Regis.UseVisualStyleBackColor = true;
            this.Regis.Click += new System.EventHandler(this.Regis_Click);
            // 
            // Del
            // 
            this.Del.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Del.Location = new System.Drawing.Point(758, 599);
            this.Del.Name = "Del";
            this.Del.Size = new System.Drawing.Size(206, 68);
            this.Del.TabIndex = 9;
            this.Del.Text = "Eliminar Vehiculo";
            this.Del.UseVisualStyleBackColor = true;
            this.Del.Click += new System.EventHandler(this.Del_Click);
            // 
            // Alq
            // 
            this.Alq.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Alq.Location = new System.Drawing.Point(1040, 599);
            this.Alq.Name = "Alq";
            this.Alq.Size = new System.Drawing.Size(151, 63);
            this.Alq.TabIndex = 10;
            this.Alq.Text = "Alquilar";
            this.Alq.UseVisualStyleBackColor = true;
            this.Alq.Click += new System.EventHandler(this.Alq_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Vehi,
            this.Prec});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(13, 123);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(261, 330);
            this.listView1.TabIndex = 11;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // ID
            // 
            this.ID.Text = "ID";
            // 
            // Vehi
            // 
            this.Vehi.Text = "Vehiculo";
            // 
            // Prec
            // 
            this.Prec.Text = "Precio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(959, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 23);
            this.label5.TabIndex = 13;
            this.label5.Text = "ID Vehiculo";
            // 
            // TxtID
            // 
            this.TxtID.Location = new System.Drawing.Point(959, 359);
            this.TxtID.Name = "TxtID";
            this.TxtID.Size = new System.Drawing.Size(190, 23);
            this.TxtID.TabIndex = 12;
            // 
            // Rec
            // 
            this.Rec.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Rec.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Rec.Location = new System.Drawing.Point(519, 489);
            this.Rec.Name = "Rec";
            this.Rec.Size = new System.Drawing.Size(226, 68);
            this.Rec.TabIndex = 14;
            this.Rec.Text = "Cargar";
            this.Rec.UseVisualStyleBackColor = true;
            this.Rec.Click += new System.EventHandler(this.Rec_Click);
            // 
            // Carro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.Rec);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtID);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.Alq);
            this.Controls.Add(this.Del);
            this.Controls.Add(this.Regis);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.TxtPrecio);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtNew);
            this.Controls.Add(this.label1);
            this.Name = "Carro";
            this.Text = "Carro";
            this.Load += new System.EventHandler(this.Carro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNew;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtPrecio;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Regis;
        private System.Windows.Forms.Button Del;
        private System.Windows.Forms.Button Alq;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Vehi;
        public System.Windows.Forms.ColumnHeader Prec;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtID;
        private System.Windows.Forms.Button Rec;
    }
}